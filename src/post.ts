export const request = (name:string) => {
    return fetch(`https://d5dd3g3chfqqqp6liral.apigw.yandexcloud.net/api/Visit/${name}`, {
        method: 'PUT',
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
}

export const requestVersion = () => {
    return fetch(`https://d5dd3g3chfqqqp6liral.apigw.yandexcloud.net/api/Version`, {
        method: 'GET',
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
}

export const requestVersionFront = () => {
    return fetch(`https://d5dd3g3chfqqqp6liral.apigw.yandexcloud.net/version.json`, {
        method: 'GET',
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
}