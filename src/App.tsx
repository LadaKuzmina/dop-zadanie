import React, {useEffect, useState} from 'react';
import './App.css'
import {request, requestVersion, requestVersionFront} from "./post";

function App() {
  const [username, setUsername] = useState('');

  const handleChange = (event:any) => {
    setUsername(event.target.value);
  };
  const [version, setVersion] = useState('');

  useEffect(() => {
    const fetchVersion = async () => {
      const response = await requestVersion();
      const versionText = await response.text();
      setVersion(versionText);
    };
    fetchVersion();
  }, []);

  const [versionFront, setVersionFront] = useState('');

  useEffect(() => {
    const fetchVersion = async () => {
      const response = await requestVersionFront();
      const versionText = await response.text();
      setVersionFront(versionText);
    };
    fetchVersion();
  }, []);

  return (
    <div className="app">
      <p>Версия: {version}</p>
      <p>Версия фронт: {versionFront}</p>
      <h1>Гостевая книга</h1>
        <p className='author'>Автор сообщения:</p>
        <input type="text" name="name" onChange={handleChange} required/>
        <br/>
        <p className='message'>Отзыв:</p>
      <textarea></textarea>
        <p>Ваш отзыв будет опубликован после проверки модератором сайта</p>
        <button onClick={()=>request(username)}>Создать</button>
    </div>
  );
}

export default App;
